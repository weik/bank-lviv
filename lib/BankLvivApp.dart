import 'package:bank_lviv/screens/homePage/homePage.dart';
import 'package:bank_lviv/screens/settingsPage/settings.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bank_lviv/screens/loginPage/login.dart';

class BankLvivApp extends StatelessWidget {
  bool lightThemeEnabled = false;

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      color: Colors.pink[800],
      debugShowCheckedModeBanner: false,
      title: 'Bank Lviv',
      theme: lightThemeEnabled
          ? ThemeData.light()
          : ThemeData(
              errorColor: Colors.red,
              // Define the default Brightness and Colors
              brightness: Brightness.dark,
              primaryColor: Colors.black,
              accentColor: Colors.pink[700],
              fontFamily: 'Montserrat',
              //primarySwatch: Colors.pink[800],
              // Define the default TextTheme.

              textTheme: TextTheme(
                headline:
                    TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
                title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
                body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
              ),
            ),
      home: LoginPage(),
      navigatorObservers: <NavigatorObserver>[observer],
      initialRoute: LoginPage.id,
      routes: {
        LoginPage.id: (context) => LoginPage(),
        HomePage.id: (context) => HomePage(analytics, observer),
        SettingsPage.id: (context) => SettingsPage(),
      },
    );
  }
}
