import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:bank_lviv/screens/homePage/homePage.dart';

class HomePageBody extends HomePage {
  var _image = 'images/background.png';

  HomePageBody(FirebaseAnalytics analytics, FirebaseAnalyticsObserver observer)
      : super(analytics, observer);

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(_image),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            child: ListView(
              /*child:_widgetOptions.elementAt(_currentIndex), that part of code should be somewhere in body*/
              children: <Widget>[
                Card(
                  color: Colors.black54,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                  child: ListTile(
                    leading: Icon(
                      Icons.credit_card,
                      color: Colors.white,
                      size: 32.0,
                    ),
                    title: Text(
                      '*4847',
                      style: TextStyle(color: Colors.white),
                    ),
                    subtitle: Text(
                      'Universal',
                      style: TextStyle(color: Colors.white),
                    ),
                    trailing: Text(
                      '1500 UAH',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
