import 'package:bank_lviv/screens/homePage/parts/body.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:bank_lviv/screens/Accounts/Accounts.dart';
import 'package:bank_lviv/screens/loginPage/login.dart';
import 'package:bank_lviv/screens/Bill_Payments/Bill_Payments.dart';
import 'package:bank_lviv/screens/Payments/Payments.dart';
import 'package:bank_lviv/screens/ExchangeRate/ExchangeRate.dart';
import 'package:flutter/src/widgets/framework.dart';


//  import 'parts/bottom.dart';
//  import 'parts/appbar.dart';
//  import 'parts/body.dart';

//import 'package:universal_widget/universal_widget.dart';

// url images from imgur for drawer

//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';

class HomePage extends StatefulWidget {

  static const String id = 'home_page';

  FirebaseAnalytics analytics;
  FirebaseAnalyticsObserver observer;

  HomePage(this.analytics, this.observer);

  @override
  HomePageState createState() => HomePageState(analytics, observer);
}

class HomePageState extends State<HomePage> {

  //int position = 0;
  var _Image = 'images/background.png';
/*static const List<Widget> _widgetOptions = <Widget>[
  myWidget1(),
  myWidget2(),                                
  myWidget3(),
];*/

  int _currentIndex;

  FirebaseAnalytics analytics;
  FirebaseAnalyticsObserver observer;

  HomePageState(this.analytics, this.observer);

  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = 0;
  }

  void _changePage(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.pink[800],
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.menu),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        actions: [
          Builder(
            builder: (context) => IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () => Scaffold.of(context).openEndDrawer(),
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Padding(
                padding: EdgeInsets.all(8.0),
              ), //Text('header', textAlign: TextAlign.center, ),
              decoration: BoxDecoration(
                color: Colors.pink[800],
                /*image: DecorationImage(
                  image: ExactAssetImage(
                    'images/logo.jpg',
                  ),
                  fit: BoxFit.contain,
                ),*/
              ),
            ),
            ListTile(
              title: Text(
                'Accounts',
                textAlign: TextAlign.start,
              ),
              subtitle: Text('Your Accounts'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => Accounts(),
                    ));
              },
            ),
            SizedBox(height: 4.0, width: 10.0),

            /*child: ListTile.builder(itemBuilder: (BuildContext context, int index) {
                return StuffInTiles(listOfTiles[index]);
                },
                itemCount: listOfTiles.length,
              ),*/
            ListTile(
              title: Text(
                'Payments',
                textAlign: TextAlign.start,
              ),
              subtitle: Text('Your Payments'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => Payments(),
                    ));
              },
            ),
            SizedBox(height: 4.0, width: 10.0),
            ListTile(
              title: Text(
                'Bill payments',
                textAlign: TextAlign.start,
              ),
              subtitle: Text('Your bill payments'),
              trailing: Icon(Icons.keyboard_arrow_right),
              //contentPadding: EdgeInsetsGeometry.lerp(ListTile, ListTile, 3.0),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => BillPayments(),
                    ));
              },
            ),
            SizedBox(height: 4.0, width: 10.0),
            ListTile(
              title: Text(
                'Exchange Rates',
                textAlign: TextAlign.start,
              ),
              subtitle: Text('Rates for currency'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => ExchangeRatePage(),
                    ));
              },
            ),
            SizedBox(
              height: 50.0,
              width: 10.0,
            ),
            /*ListTile(
               title: Text('feature', textAlign: TextAlign.start,) ,
               subtitle: Text('you need it'),
               onTap: () {Navigator.pop(context);},   
            ),*/
            Container(
                // This align moves the children to the bottom
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    // This container holds all the children that will be aligned
                    // on the bottom and should not scroll with the above ListView
                    child: Container(
                        child: Column(
                      children: <Widget>[
                          SizedBox(
                          height: 50.0,
                          ),
                          Divider(),
                          ListTile(
                            leading: Icon(Icons.help),
                            title: Text('Help and Feedback'),
                              onTap: () {
                            /*Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => HelpAndFeedback(),));*/
                          },
                        ),
                        /*ListTile(
                             leading: Icon(Icons.exit_to_app),
                             title: Text(
                                  'Log Out'), //function for exit the program, also, as i saw, in ios you can`t exit by button
                             onTap: () {
                               /*Navigator.pop(context, exit(),));*/
                             }, //usually you need to exit by yourself, as apple create, saw in stackoverflow discussion
                            ),*/
                      ],
                    ))))
          ],
        ),
      ),
      endDrawer: Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: Material(
          borderRadius:
              BorderRadius.circular(30.0), //Set this up for rounding corners.
          shadowColor: Colors.lightBlueAccent.shade100,
          child: MaterialButton(
            textColor: Colors.white,
            child: Text('Log out'),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                //navigates back to the login page
                builder: (BuildContext context) => LoginPage(),
              ),
            ),
          ),
        ),
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(_Image),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            child: ListView(
              /*child:_widgetOptions.elementAt(_currentIndex),*/
              children: <Widget>[
                Card(
                  color: Colors.black54,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                  child: ListTile(
                    leading: Icon(
                      Icons.credit_card,
                      color: Colors.white,
                      size: 32.0,
                    ),
                    title: Text(
                      '*4847',
                      style: TextStyle(color: Colors.white),
                    ),
                    subtitle: Text(
                      'Universal',
                      style: TextStyle(color: Colors.white),
                    ),
                    trailing: Text(
                      '1500 UAH',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Colors.black,
      bottomNavigationBar: BubbleBottomBar(
        opacity: 0.2,
        currentIndex: _currentIndex, //position,
        onTap: _changePage,
        /*(value){
            setState(() {
               position=value;
              });
              },*/

        borderRadius: BorderRadius.vertical(top: Radius.circular(16.0)),
        elevation: 8.0,
        fabLocation: BubbleBottomBarFabLocation.end,
        hasNotch: true,
        hasInk: true,
        inkColor: Colors.pink[800],
        items: <BubbleBottomBarItem>[
          BubbleBottomBarItem(
              backgroundColor: Colors.pink[800],
              icon: Icon(
                Icons.dashboard,
                color: Colors.black,
              ),
              activeIcon: Icon(
                Icons.dashboard,
                color: Colors.red,
              ),
              title: Text("Home")),
          BubbleBottomBarItem(
              backgroundColor: Colors.pink[800],
              icon: Icon(
                Icons.settings,
                color: Colors.black,
              ),
              activeIcon: Icon(
                Icons.settings,
                color: Colors.red,
              ),
              title: Text("Setting")),
          BubbleBottomBarItem(
              backgroundColor: Colors.pink[800],
              icon: Icon(
                Icons.announcement,
                color: Colors.black,
              ),
              activeIcon: Icon(
                Icons.announcement,
                color: Colors.red,
              ),
              title: Text("Messages")),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.pink[800],
        onPressed: () {

          analytics.logEvent(
            name: 'test_event',
            parameters: <String, dynamic>{
              'string': 'string',
              'int': 42,
              'long': 12345678910,
              'double': 42.0,
              'bool': true,
            },
          );

        },
        child: Icon(Icons.add),
      ),
    );
  }
}


/*class InTiles extends StatelessWidget {
  final MyPayments myPayments;
  InTiles(this.myPayments);

  @override
  Widget build(BuildContext context) {
    return _buildTiles(myPayments);
  }

  Widget _buildTiles(MyPayments t) {
    if (t.children.isEmpty)
      return ListTile(
          dense: true,
          enabled: true,
          isThreeLine: false,
          onLongPress: () => print("long press"),
          onTap: () => print("tap"),
          subtitle: Text("Subtitle"),
          leading: Text("Leading"),
          selected: true,
          trailing: Text("trailing"),
          title: Text(t.title));

    return ExpansionTile(
      key: PageStorageKey<int>(3),
      title: Text(t.title),
      children: t.children.map(_buildTiles).toList(),
    );
  }
}
class MyPayments {
  String title;
  List<MyPayments> children;
  MyPayments(this.title, [this.children = const <MyPayments>[]]);
}

List<MyPayments> listOfTiles = <MyPayments>[    
   MyPayments(
    'Bill Payments',
    <MyPayments>[
       MyPayments('List of payments'),
       MyPayments('Pay Bills'),
       MyPayments('Templates')
    ],
  ),
   MyPayments(
    'Payments',
    <MyPayments>[
       MyPayments('Create Payments'),
       MyPayments('List of payments'),
       MyPayments('List of templates'),
    ],
  ),
];*/



//class _NewListTile extends State {
//  final _addingListTile =     //task to create a function which will add ListTiles when a button pressed   
//}
