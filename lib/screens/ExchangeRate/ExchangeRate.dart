import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class ExchangeRatePage extends StatefulWidget {
  @override
  _ExchangeRatePageState createState() => _ExchangeRatePageState();
}

class _ExchangeRatePageState extends State<ExchangeRatePage> with RouteAware {
  final fromTextController = TextEditingController();
  List<String> currencies;
  String fromCurrency = "USD";
  String toCurrency = "GBP";
  String result;

  @override
  void initState() {
    super.initState();
    _loadCurrencies();
  }

  Future<String> _loadCurrencies() async {
    String uri = "https://api.exchangeratesapi.io/latest";
    var response = await http
        .get(Uri.encodeFull(uri), headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);
    Map currencyMap = responseBody["rates"];
    currencies = currencyMap.keys.toList();
    setState(() {});
    print(currencies);
    return "Success";
  }

  Future<String> _doConversion() async {
    String uri =
        "https://api.exchangeratesapi.io/latest?base=$fromCurrency&symbols=$toCurrency";
    var response = await http
        .get(Uri.encodeFull(uri), headers: {"Accept": "application/json"});
    var responseBody = json.decode(response.body);
    setState(() {
      result = (double.parse(fromTextController.text) *
              (responseBody["rates"][toCurrency]))
          .toString();
    });
    print(result);
    return "Success";
  }

  _onFromChanged(String value) {
    setState(() {
      fromCurrency = value;
    });
  }

  _onToChanged(String value) {
    setState(() {
      toCurrency = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Exchange rate"),
        backgroundColor: Colors.pink[800],
      ),
      body: currencies == null
          ? Center(
              child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.pink[800]),
            ))
          : Container(
              height: MediaQuery.of(context).size.height / 2,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                    elevation: 3.0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ListTile(
                          title: TextField(
                            decoration: InputDecoration(
                              fillColor: Colors.pink[800],
                              labelStyle: TextStyle(color: Colors.pink[800]),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.pink[800]),
                              ),
                            ),
                            cursorColor: Colors.pink[800],
                            controller: fromTextController,
                            style: TextStyle(
                              fontSize: 30.0,
                              color: Colors.white,
                            ),
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                          ),
                          trailing: _buildDropDownButton(fromCurrency),
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_downward),
                          onPressed: _doConversion,
                        ),
                        ListTile(
                          title: Chip(
                            label: result != null
                                ? Text(
                                    result,
                                    style: Theme.of(context).textTheme.display1,
                                  )
                                : Text(""),
                          ),
                          trailing: _buildDropDownButton(toCurrency),
                        ),
                      ],
                    )),
              ),
            ),
    );
  }

  Widget _buildDropDownButton(String currencyCategory) {
    return DropdownButton(
      value: currencyCategory,
      items: currencies
          .map((String value) => DropdownMenuItem(
                value: value,
                child: Row(
                  children: <Widget>[
                    Text(value),
                  ],
                ),
              ))
          .toList(),
      onChanged: (String value) {
        if (currencyCategory == fromCurrency) {
          _onFromChanged(value);
        } else {
          _onToChanged(value);
        }
      },
    );
  }
}
