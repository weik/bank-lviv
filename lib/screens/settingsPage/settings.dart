

import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';

//import 'package:universal_widget/universal_widget.dart';



// url images from imgur for drawer

//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';
//String nameUrl = '';

void main() => runApp( SettingsPage());

class SettingsPage extends StatefulWidget {

  static const String id = 'settings';

  @override
    SettingsPageState createState() {
      return new SettingsPageState();
    } 
}



class SettingsPageState extends State<SettingsPage> {

var lightThemeEnabled;
//SettingsPage(this.lightThemeEnabled);

//int position = 1;
int currentIndex ;

void initState() {
    // TODO: implement initState
    super.initState();
    currentIndex = 1;
  }

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
        return  Scaffold(
      appBar: AppBar(
          title: Text('Home'),
          backgroundColor: Colors.pink[800],
          
        ),
      backgroundColor:Colors.black,
      /*endDrawer:Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Material(
                borderRadius: BorderRadius.circular(30.0),//Set this up for rounding corners.
                shadowColor: Colors.lightBlueAccent.shade100,
              child:MaterialButton(
              textColor: Colors.white,
              child:Text('Re-Login'), 
              onPressed: () => Navigator.push(
                context, MaterialPageRoute(                            //navigates back to the login page
                builder: (BuildContext context) => LoginPage(),
                  ),),
                ),
              ),
            ),  */                                           
      drawer:  Drawer(
        child:  ListView(                                                                                     
          children: <Widget>[
            DrawerHeader(
              child: Padding(padding:EdgeInsets.all(8.0),),          //Text('header', textAlign: TextAlign.center, ),
                decoration:  BoxDecoration(
                color: Colors.pink[800],
                image: DecorationImage(
                  image: ExactAssetImage('images/logo.jpg', ),
                  fit: BoxFit.contain,
                ),
              ), 
            ),
             ListTile(
              title: Text('feature', textAlign: TextAlign.start,) ,
              subtitle: Text('you need it'),
              onTap: () {Navigator.pop(context);},
            ),
             ListTile(
              title: Text('feature', textAlign: TextAlign.start,) ,
              subtitle: Text('you need it'),
              onTap: () {Navigator.pop(context);},
            ),
             ListTile(
              title: Text('feature', textAlign: TextAlign.start,) ,
              subtitle: Text('you need it'),
              onTap: () {Navigator.pop(context);},
            ),
             ListTile(
              title: Text('feature', textAlign: TextAlign.start,) ,
              subtitle: Text('you need it'),
              onTap: () {Navigator.pop(context);},
            ),
            ListTile(
               title: Text('feature', textAlign: TextAlign.start,) ,
               subtitle: Text('you need it'),
               onTap: () {Navigator.pop(context);},
            ),
          ],
        ),
      ),
      body: Container(
         //decoration: BoxDecoration(
           //image: DecorationImage(
             //image: AssetImage("images/background.png"),
             //fit: BoxFit.cover,
           //),
         //),
         child:ListView(
          children: <Widget>[
            ListTile(
              title: Text('Light Theme'),
              trailing: Switch(
                value: lightThemeEnabled,
                onChanged: (changedTheme){
                  setState(() {
                    lightThemeEnabled = changedTheme;
                  });
                },
              ),
            ),
          ],
        ),
      ),

        bottomNavigationBar: BubbleBottomBar(
          opacity: .2,
          currentIndex: currentIndex,  //position,
          onTap: changePage,/*(value){
            setState(() {
               position=value; 
              });
              },*/
          borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
          elevation: 8,
          hasInk: true,
          inkColor: Colors.pink[800],
          items: <BubbleBottomBarItem>[
            BubbleBottomBarItem(
              backgroundColor: Colors.pink[800], 
              icon: Icon(Icons.dashboard,color: Colors.black,), 
              activeIcon: Icon(Icons.dashboard, color: Colors.red,), 
              title: Text("Home")
              ),
            BubbleBottomBarItem(
              backgroundColor: Colors.pink[800], 
              icon: Icon(Icons.settings,color: Colors.black,), 
              activeIcon: Icon(Icons.settings,color: Colors.red,), 
              title: Text("Setting")
              ),
            BubbleBottomBarItem(
              backgroundColor: Colors.pink[800], 
              icon: Icon(Icons.announcement,color: Colors.black,), 
              activeIcon: Icon(Icons.announcement,color: Colors.red,), 
              title: Text("Massage")
              ),
          ],
        ),
      );
        
  
  }
}

