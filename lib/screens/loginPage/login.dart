import 'package:flutter/material.dart';
import '../homePage/homePage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bank_lviv/sign_in.dart';

class LoginPage extends StatefulWidget {
  static const String id = 'login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  String nameLogo = '/images/logo.jpg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        //color: Color(0xFFAD1457),       //TRASH STUFF AS THE NEXT ONE

        decoration: BoxDecoration(
          //color: Colors.pink[800],
          color: Colors.black,
          image: DecorationImage(
            image: ExactAssetImage('images/background.png'),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
            ),
            Padding(
              padding: EdgeInsets.all(7.0),
              child: TextField(
                maxLength: 10,
                keyboardAppearance: Brightness.dark,
                cursorColor: Colors.white,
                controller: _usernameController,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                decoration: const InputDecoration(
                  //fillColor: Colors.black,
                  filled: false,
                  //helperStyle: ,
                  labelText: 'Username',
                  labelStyle: TextStyle(color: Colors.white),
                  prefixIcon: Icon(
                    Icons.person,
                    color: Color(0xFF43A047),
                  ),
                  //hintStyle: TextStyle(color: Color(0xFFAD1457)),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFAD1457)),
                  ),
                ),
                //textInputAction: TextInputAction.continueAction,
              ),
            ),

            SizedBox(
              height: MediaQuery.of(context).size.height * 0.001,
            ), //passwordField
            Padding(
              padding: EdgeInsets.all(7.0),
              child: TextField(
                keyboardAppearance: Brightness.dark,
                controller: _passwordController,
                decoration: const InputDecoration(
                  //fillColor: Colors.black,
                  filled: false,
                  labelText: 'Password',
                  labelStyle: TextStyle(color: Colors.white),
                  prefixIcon: Icon(
                    Icons.lock_outline,
                    color: Color(0xFF43A047),
                  ),
                  //suffixStyle: TextStyle(color: Color(0xFFAD1457)),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFAD1457)),
                  ),
                ),
                obscureText: true,
                //textInputAction: TextInputAction.done,
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              //common unshowed button 'Cancel' (being showed only 'cancel' text)
              children: <Widget>[
                RaisedButton(
                  //sign in with google
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      //Text('Sign in with'),
                      Icon(
                        FontAwesomeIcons.google,
                        color: Colors.pink[800],
                      ), // Icon(Icons.google)
                    ],
                  ),
                  onPressed: () {
                    signInWithGoogle().whenComplete(() {
                      Navigator.pushNamed(context,
                          HomePage.id); //navigates to the home page screen
                    });
                  },
                ),
                RaisedButton(
                  //common button 'Login'
                  color: Colors.pink[800],
                  child: Text('LOGIN'),
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.pushNamed(context,
                        HomePage.id); //navigates to the home page screen
                  },
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width / 1.72,
                  child: RaisedButton(
                    color: Colors.white,
                    splashColor: Colors.pink[800],
                    textColor: Colors.pink[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.question_answer),
                        Text(
                          'Forgot Your Password ?',
                          style: TextStyle(fontSize: 17.0),
                        ),
                      ],
                    ),
                    onPressed: () {},
                  ),
                ),
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width / 3.7,
                  child: FlatButton(
                    color: Colors.pink[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    child: Text(
                      'Sign up',
                      style: TextStyle(fontSize: 17.0),
                    ),
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

/**          ButtonBar( //common unshowed button 'Cancel' (being showed only 'cancel' text)
              children: <Widget>[
                FlatButton(
                  child: Text('CANCEL'),
                  onPressed: () {
                    _usernameController.clear();
                    _passwordController.clear();
                  },
                ),

                RaisedButton( //common button 'Login'
                  color: Colors.white,
                  child: Text('LOGIN'),
                  onPressed: () {
                    Navigator.pop(context, MaterialPageRoute(builder: (BuildContext context) => HomePage(),),);
                  },
                ),
              ],
            ), */

/**     SizedBox(height: 70),  //UsernameField
            TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                filled: true,
                labelText: 'Username',
              ),
            ), */

/**     SizedBox(height: 16.0), //passwordField
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(
                filled: true,
                labelText: 'Password',
              ),
              obscureText: true,
            ),*/

/*        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 80.0),
            Column(
              children: <Widget>[
                Image.asset('images/logo.jpg'),
                SizedBox(height: 16.0),
                Text('Bank Lviv'),
              ],
            ),
            SizedBox(height: 120.0),

            TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                filled: true,
                labelText: 'Username',
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(
                filled: true,
                labelText: 'Password',
              ),
              obscureText: true,
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text('CANCEL'),
                  onPressed: () {
                    _usernameController.clear();
                    _passwordController.clear();
                  },
                ),

                RaisedButton(
                  child: Text('NEXT'),
                  onPressed: () {
                    Navigator.pop(context, HomePage);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}*/
