import 'package:flutter/material.dart';
import 'BankLvivApp.dart';

final RouteObserver<PageRoute> routeObserver = new RouteObserver<PageRoute>();

void main() => runApp(BankLvivApp());
